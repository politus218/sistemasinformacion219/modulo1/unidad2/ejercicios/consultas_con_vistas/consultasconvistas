﻿
-- Consultas con vistas de la hoja de ejercicios 6.

  -- Crear una vista que debe mostar los pedidos de los clientes que son de Madrid, Barcelona o Zaragoza. Visualizar los campos: CÓDIGO CLIENTE, NÚMERO DE PEDIDO, EMPRESA Y POBLACIÓN. Llamar a la vista CONSULTA1.

-- La vista generada en el apartado anterior es la siguiente:

SELECT c.`CÓDIGO CLIENTE` AS `CÓDIGO CLIENTE`,
       p.`NÚMERO DE PEDIDO` AS `NÚMERO DE PEDIDO`,
       c.`EMPRESA` AS  `EMPRESA`,
       c.`POBLACIÓN` AS `POBLACION`
       
  FROM (`clientes` `c`
  JOIN `pedidos` `p`
  ON ((c.`CÓDIGO CLIENTE` = p.`CÓDIGO CLIENTE`)))
  WHERE (`c`.`POBLACIÓN` IN ('madrid', 'barcelona', 'zaragoza'))


-- Quiero modificarla para que la consulta esté optimizada. Para optimizar la consulta primero deberíamos seleccionar los registros en clientes y después combinarlos con pedidos.

